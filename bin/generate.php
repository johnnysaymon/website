<?php
require __DIR__.'/../vendor/autoload.php';

use Ciebit\DomTemplate;

$pathDestiny = __DIR__.'/../public/postagens/';
if (! file_exists($pathDestiny)) {
    mkdir($pathDestiny, 0755, true);
}

$Template = new DomTemplate;
$Template->setHtmlByFile(__DIR__.'/../src/templates/layouts/page.html');

$Files = new FilesystemIterator(__DIR__.'/../src/posts/', FilesystemIterator::CURRENT_AS_FILEINFO);

$articlesList = [];
while ($Files->valid()) {
    $Post = $Files->current();
    $PostDom = (new DomTemplate)->setHtmlByFile($Post->getRealPath());

    $Page = clone $Template;
    $Page->addElement('.content', $PostDom->getElement('.container'));
    $Page->save("{$pathDestiny}/{$Post->getFilename()}");

    $articlesList[] = [
        'title' => $PostDom->getElement('.articleTitle')->textContent,
        'description' => $PostDom->getElement('.articleDescription')->textContent,
        'datetime' => DateTime::createFromFormat(
            'Y-m-d\TH:i',
            $PostDom->getElement('.articleDate')->getAttribute('datetime')
        ),
        'url' => $Post->getFilename()
    ];

    $Files->next();
}

$Index = (new DomTemplate)->setHtmlByFile(__DIR__.'/../src/templates/layouts/post-list.html');
$PageIndex = clone $Template;

usort($articlesList, function($article1, $article2){
    return ($article1['datetime'] < $article2['datetime']) ? 1 : -1;
});

array_map(function($articleItem) use ($Index, $PageIndex) {
    $Post = $Index->getElement('.postItem')->cloneNode(true);
    $Post->setAttribute('href', "/postagens/{$articleItem['url']}");
    $Post->querySelector('.postItem__title')->textContent = $articleItem['title'];
    $Post->querySelector('.postItem__description')->textContent = $articleItem['description'];
    $Post->querySelector('.postItem__date')->textContent = $articleItem['datetime']->format('d \d\e F \d\e Y');
    $PageIndex->addElement('.content', $Post);
}, $articlesList);

$PageIndex->save(__DIR__.'/../public/index.html');
